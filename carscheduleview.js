class CarScheduleView {

	constructor() {
		// メンバ変数の定義っぽい処理
		this._scrollDiv = null;
		this._showcanvas = null;	// 表示用のキャンバス
		this._drawcanvas = null;	// 描画用のキャンバス

		this._width = 0;

		this._begin;		// 開始日
		this._maxSchedule;	// 最大スケジュール数

		this._schedule;		// スケジュール情報
		this._reserve;		// 自分の予約の連想配列

		this._days = 0;
		this._divideDays = 30;
	}

	// 1) create(element : string,width : number,height : number,begin : Date,end : Date)
	// elementで指定された要素に幅width、高さheightの範囲におさまる大きさでbeginからendの期間の初期表示をする。なお、同時に過去時間も描画する。
	create(element, begin, end) {

		if(this._scrollDiv != null){
			 this._scrollDiv.parentNode.removeChild(this._scrollDiv);
		}

		// 親divの取得
		var parentDiv = document.getElementById(element);
		if(parentDiv == null){
			// ないよ
			return;
		}

		// スクロールバーの幅を取得
		var tester = document.createElement('div');
		tester.style.bottom = '100%';
		tester.style.height = '1px';
		tester.style.position = 'absolute';
		tester.style.width = 'calc(100vw - 100%)';
		parentDiv.appendChild(tester);
		var scrollbarWidth = window.getComputedStyle(tester, null).getPropertyValue('width');
		parentDiv.removeChild(tester);

		// スクロールのdivを作成
		var width = Math.floor(parentDiv.clientWidth / 3) * 3;
		var height = Math.ceil(width / 4);
		var scrollDiv = document.createElement('div');
		scrollDiv.style.width = String(width) + 'px';
		scrollDiv.style.height = String(height + scrollbarWidth) + 'px';
		scrollDiv.style.backgroundColor = 'white'; 
		scrollDiv.style.marginLeft = 'auto';
		scrollDiv.style.marginRight = 'auto';
		scrollDiv.style.overflow = 'auto';
		scrollDiv.style.border = 'none';
		scrollDiv.style.padding = '0px';
		parentDiv.appendChild(scrollDiv);
		this._scrollDiv = scrollDiv;

		// 日数を算出
		var days = (end.getTime() - begin.getTime()) / (1000 * 60 * 60 * 24) + 1;
		if(days < 3){
			days = 3;	// 最低でも３日分は描画する
		}
		var canvasCount = Math.ceil(days / this._divideDays);

		var childDiv;
		if(canvasCount > 1){
			childDiv = document.createElement('div');
			childDiv.style.width = String((width * days) / 3) + 'px';
			childDiv.style.height = String(height + scrollbarWidth) + 'px';
			scrollDiv.appendChild(childDiv);
		}
		else{
			childDiv = scrollDiv;
		}

		// canvasの作成と解像度に対するスケーリング
		// https://developer.mozilla.org/ja/docs/Web/API/Window/devicePixelRatio
//		var scale = window.devicePixelRatio;
		var scale = 1;	// Androidの描画性能劣化を理由に対応しない

		this._showcanvas = new Array(canvasCount);
		this._drawcanvas = new Array(canvasCount);
		var date = new Date(begin.getTime());
		for(var i = 0; i < canvasCount; i++){
			var dayCount = days - (i * this._divideDays);
			if(dayCount > this._divideDays){
				dayCount = this._divideDays;
			}

			// 表示用キャンバス作成
			var showcanvas = document.createElement('canvas');
			showcanvas.width = ((width * dayCount) / 3) * scale;
			showcanvas.height = height * scale;
			showcanvas.style.width = String((width * dayCount) / 3) + 'px';
			showcanvas.style.height = String(height) + 'px';
			childDiv.appendChild(showcanvas);
			this._showcanvas[i] = showcanvas;

			// 描画用キャンバス作成（Android端末のためプリレンダリングを採用）
			var drawcanvas = document.createElement('canvas');
			drawcanvas.width = ((width * dayCount) / 3) * scale;
			drawcanvas.height = height * scale;
			this._drawcanvas[i] = drawcanvas;

			// 2Dのコンテキストを取り出す
			var context = drawcanvas.getContext('2d');

			// スケーリング
			context.scale((width / 864.0) * scale, (height / 216.0) * scale);

			context.fillStyle = '#ffffff';
			context.fillRect(0, 0, 288 * dayCount, 260);

			// ラベルの背景を塗りつぶし
			context.fillStyle = '#eef5d3';
			context.fillRect(0, 1, 288 * dayCount, 69);

			// ラベルの枠を描画
			context.strokeStyle = '#5c343c';
			context.beginPath();
			for(var j = 0; j < dayCount + 1; j++){
				context.moveTo(j * 288, 1);
				context.lineTo(j * 288, 70);
			}
			context.stroke();
			context.strokeStyle = '#808080';
			context.beginPath();
			context.moveTo(0, 1);
			context.lineTo(288 * dayCount, 1);
			context.stroke();

			// 日付を描画
			context.fillStyle = '#808080';
			context.font = "32px sans-serif";
			for(var j = 0; j < dayCount; j++, date.setDate(date.getDate() + 1)){
				var dateLabel = '　' + (date.getMonth() + 1) + '月' + date.getDate() + '日（' + '日月火水木金土'[date.getDay()] + '）';

				context.fillText(dateLabel, j * 288 + ((288 - context.measureText(dateLabel).width) / 2), 50);
			}

			// 時間を描画
			context.font = "24px sans-serif";
			for(var j = 0; j < dayCount; j++){
				context.fillText("0:00",  j * 288, 211);
			}
			for(var j = 0; j < dayCount; j++){
				context.fillText("12:00",  114 + j * 288, 211);
			}
		}

		this._width = width;

		this._begin = begin.getTime();
		this._maxSchedule = days * 24 * 6;

		this._days = days;

		this._schedule = new Array();

		// スケジュールをクリア
		this.clear();

		// スケジュールを描画
		this.update();
	}

	// 2) add(schedule : {begin : Date,end : Date,status : number}[])
	// scheduleで指定されたスケジュールを追加する。ここには自分の予約予定は含まれない。スケジュール期間に過去時間が含まれる場合は過去時間の範囲は無視する。
	// 描画は更新しない。 
	add(schedule) {
		for(var j = 0; j < schedule.length; j++){
			// 開始時間を取得
			schedule[j].begin = Math.floor((schedule[j].begin.getTime() - this._begin) / (1000 * 60 * 10));
			// 終了時間を取得
			schedule[j].end = Math.ceil((schedule[j].end.getTime() - this._begin) / (1000 * 60 * 10));

			if(schedule[j].begin >= this._maxSchedule || schedule[j].end < 0){
				continue;
			}
			if(schedule[j].status == 3){
				this._reserve = schedule[j];	// 自分の予約の場合は情報を保持（無いはずらしい）
			}
			else{
				this._schedule.push(schedule[j]);
			}
		}
	}

	// 3) setMy(schedule : {begin : Date,end : Date,status : number})
	// scheduleで指定されたスケジュールを自分の予約として設定する。常に1期間しか指定できないものとして既に設定されている現在値と差し替える。描画は更新しない。
	setMy(schedule) {
		// 開始時間を取得
		schedule.begin = Math.floor((schedule.begin.getTime() - this._begin) / (1000 * 60 * 10));
		// 終了時間を取得
		schedule.end = Math.ceil((schedule.end.getTime() - this._begin) / (1000 * 60 * 10));

		if(schedule.begin < this._maxSchedule && schedule.end >= 0){
			this._reserve = schedule;
		}
		else{
			this._reserve = null;
		}
	}

	// 4) clear()
	// 現在の設定スケジュールを全てクリアする。描画は更新しない。
	clear() {
		this._schedule = new Array();
		this._reserve = null;
	}

	// 5) jump(date : Date)
	// dateで指定された日が一番左に来るようにスクロールする。
	jump(date) {
		var day = (date.getTime() - this._begin) / (1000 * 60 * 60 * 24);
		if(day < 0){
			day = 0;
		}

		this._scrollDiv.scrollLeft = (this._width * day) / 3;
	}

	// 6) update()
	// 描画を更新する。 
	update() {
		var fillStyleArray = ['#ffffff', '#666666', '#bbbbbb', '#00a33c', '#e53935', '#a7db9d5'];
//@@		var fillStyleArray = ['#ffffff', '#FF0000', '#008000', '#0000FF', '#FFFF00', '#FF00FF'];

		// 現在の時間から過去時間を設定
		var now = Math.ceil(((new Date()).getTime() - this._begin) / (1000 * 60 * 10));

		// 自分の予約時間を取得
		var reserveStart = 0;
		var reserveEnd = 0;
		if(this._reserve != null){
			reserveStart =  this._reserve.begin;
			reserveEnd = this._reserve.end;
		}


		for(var i = 0; i < this._drawcanvas.length; i++){
			var context = this._drawcanvas[i].getContext('2d');
			var dayCount = this._days - (i * this._divideDays);
			if(dayCount > this._divideDays){
				dayCount = this._divideDays;
			}

			var schedule = new Array(dayCount * 24 * 6);
			schedule.fill(0);

			// スケジュールを設定
			for(var j = 0; j < this._schedule.length; j++){
				var begin = this._schedule[j].begin - dayCount * 24 * 6 * i;
				var end = this._schedule[j].end - dayCount * 24 * 6 * i;

				if(begin < schedule.length && end >= 0){
					schedule.fill(this._schedule[j].status, begin < 0 ? 0 : begin, end > schedule.length ? schedule.length : end);
				}
			}

			// 過去時間を設定
			if(now > 0){
				schedule.fill(1, 0, now > schedule.length ? schedule.length : now);
			}

			// 自分の予約の設定
			if(reserveStart < schedule.length && reserveEnd >= 0){
				var begin = reserveStart < 0 ? 0 : reserveStart;
				var end = reserveEnd > schedule.length ? schedule.length : reserveEnd;
				for(j = begin; j < end; j++){
					schedule[j] += 3;
				}
			}

			// 予定を描画
			var fillStyle = schedule[0];
			var left = 0;
			var width = 0;
			for(var j = 0; j < schedule.length; j++){
				if(fillStyle != schedule[j]){
					context.fillStyle = fillStyleArray[fillStyle];
					context.fillRect(left, 70, width, 116);
					left += width;
					width = 0;
					fillStyle = schedule[j];
				}
				width += 2;
			}
			if(width > 0){
				context.fillStyle = fillStyleArray[fillStyle];
				context.fillRect(left, 70, width, 116);
			}

			// 枠を描画
			// １時間単位
			context.lineWidth = 0.5;
			context.strokeStyle = '#e3e3e3';
			context.beginPath();
			for(var j = 1; j < dayCount * 24; j++){
				context.moveTo(j * 12, 70);
				context.lineTo(j * 12, 186);
			}
			context.stroke();

			// １２時間単位
			context.lineWidth = 1;
			context.strokeStyle = '#5c343c';
			context.beginPath();
			for(var j = 0; j < dayCount * 2 + 1; j++){
				context.moveTo(j * 144, 70);
				context.lineTo(j * 144, 186);
			}
			context.stroke();

			context.strokeStyle = '#808080';
			context.beginPath();
			context.moveTo(0, 70);
			context.lineTo(288 * dayCount, 70);
			context.moveTo(0, 186);
			context.lineTo(288 * dayCount, 186);
			context.stroke();

			now -= dayCount * 24 * 6;
			reserveStart -= dayCount * 24 * 6;
			reserveEnd -= dayCount * 24 * 6;

			// プリレンダリングした内容を表示用キャンバスに表示
			this._showcanvas[i].getContext('2d').drawImage(this._drawcanvas[i], 0, 0);
		}
	}
}
